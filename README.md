# ML-project

**Project name:** Rock, Paper, Scissors, Lizard, Spock
<br/>
<br/>
**Authors:** Jovana Radjenovic, Zorana Aleksic
<br/>
<br/>
**Project description:** Recreating the "Rock, Paper, Scissors, Lizard, Spock" game from "The Big Bang Theory" series. The game: https://youtu.be/x5Q6-wMx-K8. Idea: The computer camera records the player. The player puts his hand into the square marked on the screen. Based on the shape that the player formed with his hand, and the shape the computer chose by random selection, the winner is chosen according to the rules of the game.
<br/>
<br/>
**Dataset description:** Images for all shapes were taken with [Python script](https://github.com/SouravJohar/rock-paper-scissors/blob/master/gather_images.py?fbclid=IwAR2b7Anq5vXhFP_A-YYDegejXPx6B8IkiH-tDFeYZIqVKFd3D9SjOxGvfjU) (with a small change: reverse mirror effect). The dataset consists of 3600 images, 600 images for each class.
<br/>
<br/>
**Recommendation:** Play game in front of a white surface, such as a wall.

<br/>
<br/>
For more information see wiki page.
